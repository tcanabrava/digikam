FlatPak build are processed daily by the continuous integration script from KDE Jenkinks server.
Bundle must be deployed automatically on Discover Kdeapps testing source repository.

- Status;        https://binary-factory.kde.org/view/Flatpak/job/Digikam_flatpak/
- Configuration: https://invent.kde.org/kde/flatpak-kde-applications/-/blob/master/org.kde.digikam.json
- Repository:    https://distribute.kde.org/kdeapps.flatpakrepo
- Discover:      https://userbase.kde.org/Discover

[![](https://i.imgur.com/IHxNhDT.png "Settings Kdeapps testings source repository in Discovery Application")](https://imgur.com/IHxNhDT)
[![](https://i.imgur.com/KWNjWBe.png "digiKam FlatPak bundle Properties in Discovery Application")](https://imgur.com/KWNjWBe)
